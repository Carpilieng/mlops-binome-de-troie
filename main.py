import uvicorn
from json import dumps
from fastapi import FastAPI
import logging
import spacy
import os

logging.info("Loading model..")
nlp = spacy.load("./models")
app = FastAPI()

@app.get("/api/intent")
def intent_inference(sentence: str):
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return dumps(inference.cats)


@app.get("/api/intent-supported-languages")
def supported_languages():
    return dumps(["fr-FR - Francais"])


@app.get("/health", status_code=200)
def health_check_endpoint():
    pass

@app.get("/test")
def test():
    return dumps(["This is a test page"])

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=22222)