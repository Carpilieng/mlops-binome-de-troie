FROM python:3.7-buster
COPY requirements.txt /tmp/requirements.txt
COPY main.py ./
ADD models ./models 
RUN pip install -r /tmp/requirements.txt

CMD ["gunicorn", "main:app", "--preload", "--worker-class", "uvicorn.workers.UvicornWorker"]