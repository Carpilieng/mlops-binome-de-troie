from main import supported_languages, intent_inference

def test_supported_langage():
    """Check that supported langage return fr."""
    resp = supported_languages()
    assert '["fr-FR - Francais"]' == resp

def test_intent_inference_size():
    """Check that intent_inference return a string."""
    resp = intent_inference("This is a test")
    assert type(resp) == str
    assert len(resp) >= 3

def test_intent_inference_tags():
    """Check that every tags is in the results."""
    resp = intent_inference("I want to eat in a restaurant")
    assert "purchase" in resp
    assert "find-restaurant" in resp
    assert "irrelevant" in resp
    assert "find-hotel" in resp
    assert "provide-showtimes" in resp
    assert "find-around-me" in resp
    assert "find-train" in resp
    assert "find-flight" in resp

def test_intent_inference_content():
    """Check that basic intention can be detected."""
    resp = intent_inference("I want to eat in a restaurant")
    # Get index of the beginning of the value
    ind = resp.find("find-restaurant")+18
    assert float(resp[ind:ind+8]) > 0.2