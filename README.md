https://tinyurl.com/mlops-paper

"Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?"
* L'image docker a une taille de 422.91 MiB. La limite de RAM imposée par Heroku est de 512 Mo, et nous recommandons donc de définir la limite de RAM à cette valeur.

"Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms"
* Nous dépassons les 200 ms lorsque nous dépassons les 600 requêtes par secondes pour le P95. Locust ne nous propose pas une manière efficace de visionner le P99. Nous vous fournissons des captures d'écran pour observer l'évolution du temps de réponse.

"On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?"
* Nous proposons de mettre dans des batchs les différentes données reçues par le modèle, afin de traiter plusieurs données à chaque inférence du modèle.